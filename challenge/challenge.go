package challenge

import (
	"errors"
	"github.com/astaxie/beego"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"
)

type Challenge struct {
	Number     int `yaml:"number"`
	Difficulty int `yaml:"difficulty"`
	//Answer     string `yaml:"answer"` -- This will be provided by encrypted yaml
	Name string `yaml:"name"`
	RealName string
	Desc string `yaml:"desc"`
	Body string //-- This will be provided by the readme
}

var labpath string = beego.AppConfig.String("labpath")

var yamlanswerpath string = beego.AppConfig.String("yamlanswerpath")

var challenges map[int]Challenge

var ErrChallengeNotFound = errors.New("challenge not found")

// Gets all challenges and puts them into database.
func FetchLoop() error {

	challenges = make(map[int]Challenge)

	beego.Debug("Starting fetchloop for challenge")
	for {

		err := filepath.Walk(labpath, walkfunc)

		if err != nil && err != filepath.SkipDir {
			beego.Error("Error parsing labyrinth!")
		}

		time.Sleep(10 * time.Minute)

	}
	return errors.New("Challenges no longer being fetched")

}

func walkfunc(path string, info os.FileInfo, err error) error {

	// do I need this?
	if err != nil {
		beego.Error(err)
		return err
	}

	if info.IsDir() && info.Name() == ".git" {
		beego.Debug("Skipped", info)
		return filepath.SkipDir
	}

	// TODO put this in configuration
	if strings.HasSuffix(path, ".labfile") {
		// Time to add things to the challenge
		challengebytes, err := ioutil.ReadFile(path)
		var challengestruct Challenge


		// Get the readme in this directory
		readmepath := filepath.Dir(path) + "/README" // UNIX only I guess
		readmebytes, err := ioutil.ReadFile(readmepath)
		if err != nil { // No README or problems reading it
			//beego.Warn("Found labfile", path, "but no readme, skipping")
			return filepath.SkipDir
		}

		err = yaml.Unmarshal(challengebytes, &challengestruct)
		challengestruct.Body = string(readmebytes)
		challengestruct.RealName = filepath.Base(filepath.Dir(path))
		if err != nil { //TODO: Make sure it just skips over this
			beego.Error("Error parsing challenge yaml:", err)
			return err
		}

		challenges[challengestruct.Number] = challengestruct
	}

	return nil

}

func GetAnswer(answernum int) (string, error) {

	answerbytes, err := ioutil.ReadFile(yamlanswerpath)
	if err != nil {
		beego.Error("Cannot open yaml answers:", err)
		return "", err
	} else if len(answerbytes) <= 0 {
		beego.Error("Answer file has length of 0")
		return "", errors.New("empty answer file")
	}

	var answermap map[int]string
	err = yaml.Unmarshal(answerbytes, &answermap)
	if err != nil {
		beego.Error("Error unmarshalling answers:", err)
		return "", err
	}

	return answermap[answernum], nil

}

func GetAllChallenges() map[int]Challenge {

	return challenges

}

func GetChallenge(number int) (Challenge, error) {

	for _, challenge := range challenges {
		if challenge.Number == number {
			return challenge, nil
		}
	}
	return Challenge{}, ErrChallengeNotFound
}
