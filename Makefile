PKGS := $(shell go list ./... | grep -v /vendor)

LABYRINTH_RUN_MODE := dev
FIXTURE := $(LABYRINTH_RUN_MODE)

test:
	go test $(PKGS)

install_fixture:
	cp -a fixtures/$(FIXTURE)/conf/* conf/
	cp -a fixtures/$(FIXTURE)/lab.db .

run:
	go run main.go

.PHONY: test run install_fixture
