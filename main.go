package main

import (
	"github.com/astaxie/beego"
	"gitlab.com/uwmcomputersociety/labyrinth-site/challenge"
	"gitlab.com/uwmcomputersociety/labyrinth-site/controllers"
	"gitlab.com/uwmcomputersociety/labyrinth-site/player"
	_ "gitlab.com/uwmcomputersociety/labyrinth-site/routers"
	"gitlab.com/uwmcomputersociety/labyrinth-site/ssh"
	"gitlab.com/uwmcomputersociety/labyrinth-site/users"
)

func main() {

	beego.ErrorController(&controllers.ErrorController{})

	go player.Init()
	go ssh.ServerInit()
	go users.DBLoop()
	go challenge.FetchLoop()

	beego.AddFuncMap("UidToName", users.UidToName)
	beego.Run()
}
