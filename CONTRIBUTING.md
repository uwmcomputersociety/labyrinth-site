Why not contribute to this project? Any contribution is appreciated,
and I'll find any way possible to help you. Seriously, just hack away
at something you think should be changed/added!

## Contributing Guidelines

1. Read the README.
1. Spend at least 30 minutes of your life reading this code, building
   it, and tinkering a bit.
1. Find some lowhanging fruit like typos, poor optimization, or values
   that could be put into configuration. Or tackle something on the
   "todos" in the readme if you're feeling ambitious.
1. MAKE A FORK!!! Or a branch if it's a new feature or big change.

## Pull Request Guidelines

Specify the following:

- Brief summary of the changes you made
- What packages this effects (packages = folders within the project)
- Anything that could possibly go wrong with these changes being
  implemented
- Any testing that you did to make sure these changes wouldn't break
  anything
