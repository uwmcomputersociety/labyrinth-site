<html>
<head>

<link rel="stylesheet" href="/static/css/minstyle.css" type="text/css" media="screen">

</head>
<body>

{{ .Header }}

<h3>{{ .Challenge.Number }}: {{ .Challenge.Name }} <a href="{{ .LabRepo }}">⎆</a></h3>
<h3>Difficulty: {{ .Challenge.Difficulty }}</h3>
<h3>{{ .Challenge.Desc }}</h3>

<div id="challengebody">
	<pre>{{ .ChallengeBody }}</pre>
</div>

<h3>Completed:</h3>
<ul>
	<table id="challengetable">
	<tr>
		<th><!--Completed?--></th>
		<th>User</th>
		<th>Attempts</th>
	<tr>
	{{ range $key, $value := .AttemptedChallenges }}
	<tr>
		<td>{{ if eq $value.Completed true }}۞{{ end }} </td>
		<td>{{ $key | UidToName }}</td>
		<td>{{ $value.Attempts }}</td>
	</tr>
	{{ end }}
</ul>

</body>
</html>
