<html>
<head>
	<link rel="stylesheet" href="/static/css/minstyle.css" type="text/css" media="screen">
</head>

<body>
{{ .Header }}

<h2>Where am I?</h2>
<p>You have entered the <strong>wizard's tower.</strong> Here lie many challenges that will test your programming ability.</p>

<h2>How do I begin?</h2>
<p><strong>Currently participation is limited to the UWM Computer Society.</strong> If you are a member of this organization, please follow the instructions in the <a href="https://gitlab.com/uwmcomputersociety/compsoc-ansible/raw/master/playbooks/vars/userlist.yml">userlist.yml</a> file.</p>
<p>Once you have been added to this list, wait about 20 minutes for the server to initilize users from this file and you will be added as a player. Now you can begin submitting answers.</p>
<p>All challenges are referencing a challenge on the the <a href="http://gitlab.com/uwmcomputersociety/labyrinth">uwmcomputersociety/labyrinth repo</a>. It is recommend that you clone this to work on the challenges, but you can also click the  ⎆ icon on each challenge page for a direct link to the folder in the gitlab repo.</p>
<p>Answers are stored in a hidden file on this server and are checked against submissions. If you submit the correct answer, you will earn points equalling the difficulty of the challenge.</p>

<h2>Answer submission</h2>
<p>To submit answers, enter the challenge number followed by your flag as an argument to the ssh call.</p>
<p>For example, if this site is 'example.com', you would submit by with <code>ssh example.com -p 3997 4 thisisaflag</code> The '3997' is the port, '4' is the challenge number, and 'thisisaflag' is the flag.</p>
</body>
</html>
