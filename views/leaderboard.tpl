<html>
<head>
	<link rel="stylesheet" href="/static/css/minstyle.css" type="text/css" media="screen">
</head>
<body>
{{ .Header }}

<table id="leaderboardtable">
	<tr>
		<th></th>
		<th>User</th>
		<th>Score</th>
	</tr>
	{{ range .Leaderboard }}
	<tr>
		<td>{{ .Place }}</td>
		<td>{{ .Name }}</td>
		<td>{{ .Score }}</td>
	</tr>
	{{ end }}
</table>
		
</body>
</html>
