<html>
<head>
	<link rel="stylesheet" href="/static/css/minstyle.css" type="text/css" media="screen">
</head>

<body>
{{ .Header }}
<h1>Challenge List</h1>

{{ range .ChallengeList }}
<div id="challengeitem">
	<h2><a href="/challenges/{{ .Number }}">{{ .Number }}: {{ .Name }}</a> [{{ .Difficulty}}]</h2>
	<p>{{ .Desc }}</p>
	<hr>
</div>
{{ end }}

</body>
</html>
