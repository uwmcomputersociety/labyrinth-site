package users

import (
	"github.com/astaxie/beego"
	"github.com/boltdb/bolt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"net/http"
	"time"
)

type CompsocUsers struct {
	Users []User `yaml:"compsocusers"`
}

type User struct {
	Name    string `yaml:"name"`
	Uid     string `yaml:"uid"`
	Comment string `yaml:"comment"`
	State   string `yaml:"state"`
	Pubkey  string `yaml:"pubkey"`
}

// When FetchData runs successfully, it puts all users into this variable.
// It is a staging variable and is ultimately put into the UserDatabase.
var users CompsocUsers

var LastUpdated time.Time

func DBLoop() {

	//testdb()
	for {
		FetchData()
		time.Sleep(time.Minute * 30)
	}

}

func FetchData() {

	resp, err := http.Get(beego.AppConfig.String("userlisturl"))
	if err != nil {
		beego.Warning("Cannot reach userlisturl.")
		return
	}
	defer resp.Body.Close()

	txt, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		beego.Error("Cannot read response body to text!")
		return
	}

	err = yaml.Unmarshal([]byte(txt), &users)
	if err != nil {
		beego.Error("Unmarshal Error:", err, "-- this probably means the url isn't linking to correct yaml.")
		//beego.Debug(string(txt)) // prints webpage

		return
	}

	// Open Database
	UserDatabase, err := bolt.Open("lab.db", 0600, &bolt.Options{Timeout: 5 * time.Second})
	if err != nil {
		beego.Error(err)
	}
	defer UserDatabase.Close()

	for _, user := range users.Users {
		err = UserDatabase.Update(func(tx *bolt.Tx) error {
			b, err := tx.CreateBucketIfNotExists([]byte("USERS"))
			if err != nil {
				return err
			}

			userbytes, err := yaml.Marshal(&user)
			if err != nil {
				beego.Error("Marshal error: ", err)
			}

			err = b.Put([]byte(user.Uid), []byte(userbytes))
			return err
		})
		if err != nil {
			beego.Warning(err)
		}
	}

}

func testdb() {

	// Open DB
	UserDatabase, err := bolt.Open("lab.db", 0600, &bolt.Options{Timeout: 5 * time.Second})
	if err != nil {
		beego.Error(err)
	}
	defer UserDatabase.Close()

	// Read all entries
	count := 0
	err = UserDatabase.Update(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b, err := tx.CreateBucketIfNotExists([]byte("USERS"))
		if err != nil {
			beego.Error(err)
			return err
		}

		c := b.Cursor()

		for k, _ := c.First(); k != nil; k, _ = c.Next() {
			count++
		}
		beego.Info("DB seems fine, userdata contains", count, "users.")

		return nil
	})

}
