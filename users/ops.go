package users

import (
	"errors"
	"fmt"
	"github.com/astaxie/beego"
	"github.com/boltdb/bolt"
	"github.com/gliderlabs/ssh"
	gossh "golang.org/x/crypto/ssh"
	"gopkg.in/yaml.v2"
	"log"
	"time"
)

var ErrUserDoesNotExist = errors.New("user does not exist")

// Gets the user by the uid. Returns nil if there is no matches.
func GetUserByUid(uid string) (User, error) {

	// Open DB
	UserDatabase, err := bolt.Open("lab.db", 0600, &bolt.Options{Timeout: 2 * time.Second})
	if err != nil {
		log.Fatal(err)
	}
	defer UserDatabase.Close()

	var user User
	err = UserDatabase.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("USERS"))
		u := b.Get([]byte(uid))
		if u == nil {
			beego.Error("ERROR, uid", uid, "has no user")
			return ErrUserDoesNotExist
		}
		err := yaml.Unmarshal(u, &user)
		if err != nil {
			beego.Error("Error unmarshalling user")
		}
		return nil
	})

	return user, err

}

// From a pubkey, get the associated user's uid
// Returns nil if there is no associated user.
func GetUserByPubkey(pubkey ssh.PublicKey) User {

	var user User
	var retuser User

	// Open DB
	UserDatabase, err := bolt.Open("lab.db", 0600, &bolt.Options{Timeout: 2 * time.Second})
	if err != nil {
		beego.Error("Error opening database")
	}
	defer UserDatabase.Close()

	UserDatabase.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket([]byte("USERS"))

		c := b.Cursor()

		var ukey ssh.PublicKey
		for k, v := c.First(); k != nil; k, v = c.Next() {
			err := yaml.Unmarshal([]byte(v), &user)

			ukey, _, _, _, err = gossh.ParseAuthorizedKey([]byte(user.Pubkey))
			if err != nil {
				beego.Error("Error parsing user key")
			}

			if ssh.KeysEqual(ukey, pubkey) {
				if retuser.Uid != "" {
					beego.Warning("Duplicate pubkey for user", user.Name)
				}
				retuser = user
			}
		}
		return err
	})

	return retuser
}

func UidToName(uid string) string {

	user, err := GetUserByUid(uid)
	if err != nil {
		return "nil"
	} else {
		return user.Name
	}

}

// TODO: Put this in a "users" package.
// Returns an array of User structs, representing all users in the database.
// This can be iterated through in html templates
func GetAllUsers() []User {

	var userlist []User
	var user User

	UserDatabase, err := bolt.Open("lab.db", 0600, &bolt.Options{Timeout: 2 * time.Second})
	if err != nil {
		beego.Error("Error opening database")
	}
	defer UserDatabase.Close()

	UserDatabase.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("USERS"))
		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			err := yaml.Unmarshal([]byte(v), &user)
			if err != nil {
				beego.Error("Error unmarshalling user")
			}

			userlist = append(userlist, user)
		}
		return err
	})

	return userlist
}

// TODO: Put this in a "users" package
func PrettyPrint(user User) string {

	return fmt.Sprintf(`
%s (UUID: %s):
- username: %s
- state: %s
- pubkey: %s
`, user.Comment, user.Uid, user.Name, user.State, user.Pubkey)
}
