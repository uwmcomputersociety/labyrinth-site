package controllers

import (
	"github.com/astaxie/beego"
	"gitlab.com/uwmcomputersociety/labyrinth-site/challenge"
	"gitlab.com/uwmcomputersociety/labyrinth-site/users"
	"gitlab.com/uwmcomputersociety/labyrinth-site/player"
	"html/template"
	"io/ioutil"
	"sort"
	"strconv"
)

func getDefaults(c *MainController) {
	c.Data["Header"] = InsertHTML("header.tpl")
}

type ErrorController struct {
	beego.Controller
}
type MainController struct {
	beego.Controller
}

func (c *MainController) Get() {
	getDefaults(c)
	c.TplName = "index.tpl"
}

func (c *MainController) About() {
	getDefaults(c)
	c.TplName = "about.tpl"
}

func (c *MainController) Challenges() {
	getDefaults(c)
	c.TplName = "challenges.tpl"

	challengenum := c.Ctx.Input.Param(":challenge")

	// Show homepage if there's no /:challengnumber given
	if challengenum == "" {
		c.Data["ChallengeList"] = challenge.GetAllChallenges()
		return
	}

	// 404 if any problems parsing challenge number
	chal, err := strconv.ParseInt(challengenum, 10, 32)
	if err != nil {
		c.Abort("404")
	}

	// 404 if challenge doesn't exist
	challengestruct, err := challenge.GetChallenge(int(chal))
	if err != nil {
		c.Abort("404")
	}

	c.Data["AttemptedChallenges"], err = player.GetChallenges(int(chal))

	if err != nil {
		beego.Debug("Error getting challenge or something")
		c.Data["ChallengeList"] = challenge.GetAllChallenges()
		if err != nil {
			beego.Error("error getting all completed challenges", err)
		}
		return
	}
	c.Data["Challenge"] = challengestruct
	c.Data["ChallengeBody"] = challengestruct.Body
	c.Data["LabRepo"] = beego.AppConfig.String("baselabyrinthrepo") + challengestruct.RealName

	c.TplName = "challenge.tpl"

}

func (c *MainController) Leaderboard() {
	getDefaults(c)
	c.TplName = "leaderboard.tpl"

	allusers := users.GetAllUsers() // TODO: Change this to an "get all players"

	var lb []*player.LeaderboardEntry
	for _, u := range allusers {
		score, err := player.GetPlayerScore(u.Uid)
		if err != nil {
			beego.Error(err)
			continue
		}
		lb = append(lb, &player.LeaderboardEntry{
			Name:  u.Name,
			Score: score,
			//Place: -1, //placeholder
		})
	}

	sort.Slice(lb, func(i, j int) bool {
		return lb[i].Score > lb[j].Score
	})

	if len(lb) <= 0 {
		beego.Warning("Zero-length leaderboard: this shouldn't be happening")
		return // Avoid nil problems
	}

	var place int = 1
	var prevscore int = lb[0].Score // Set the initial score to the first score
	for _, l := range lb {
		if prevscore != l.Score {
			place++
			prevscore = l.Score
		}
		l.Place = place
	}

	c.Data["Leaderboard"] = lb

}

// 404 Page
func (c *ErrorController) Error404() {
	c.TplName = "404.tpl"
}

// `path` is a bit of html in the 'views' directory.
// For example a path to "/views/test.tpl" would be "test.tpl"
func InsertHTML(path string) template.HTML {
	file, err := ioutil.ReadFile("views/" + path)
	if err != nil {
		beego.Error(err)
	}
	return template.HTML(string(file))
}
