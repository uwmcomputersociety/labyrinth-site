This is a golang/beego app that will host the labyrinth. It handles
both recording the users and the challenges, as well as displaying
them in a html view.

Users can issue commands to the server using ssh for submitting
answers. At some point it will allow for viewing challenges or
leaderboards.

## The Labyrinth

The labyrinth is a mystical-themed series of coding challenges that
will both showcase a player's programming ability and improve their
coding skills. The player has entered a magical tower and will
complete a series of challenges, submitting the answers by ssh. Their
identity is pulled from our compsoc-ansible ansible playbook
repo. Each challenge has a difficulty. A player's score is the sum of
the difficulties of the challenges which they have completed.

All challenges are going to come from the labyrinth repo. This will
soon be added as a git submodule to this directory. This repo will be
parsed to create the challenges. The player will be recommended to
clone the repo, or perhaps SSH into the server to view the challenges.

Each challenge is in a directory named with a `challengenumber-name`
convention. Each directory contains the following files:

- README: Describes the challenge and is only parsed to provide
  instructions for the challenge, including how to find the
  answer. This should be formatted as plain text and will be displayed
  in monospace font.
- .labfile: A yaml-formatted file that contains the name of the
  challenge, the difficulty, a short description (desc), and the
  number. If there is no .labfile in the directory, it will not be
  parsed and added to the list of challenges.
- challenge files: misc. files and code that are used to solve the
  challenges.

### Database Structure

Currently users and players are stored in a boltdb instance in the
root directory of this repo. This db organizes binary blogs into
buckets referenced by keys. The following is the structure of the db:

- "USERS" contains users pulled from the uwmcomputersociety ansible
  variables that have the member's usernames, pubkeys, etc. These are
  stored as yaml and indexed by the uid of the user.
- "PLAYERS" contains buckets refereneced by uid, and within these
  buckets is PlayerChallenges referenced by the number of the
  challenge. PlayerChallenges are only created upon the first attempt.

It is the reponsibility of the host to backup this database
periodically.

## Hosting

I am making it an aim of this project to be as easy to host for anyone
interested. This means making it easy to configure and to have clear
error messages.

Most configuration is in the `conf/app.conf` directory. Please take a
look at this file as it contains detailed information on which files
need to be generated, managed, and hidden from others.

## TODO:

### Usability

- Allow commands to be issued to the server to do things like CRUD
  operations on users. Admins could be defined with an authorized_keys
  file.

### Registration

- Allow people to sign-up by simply submitting answers -- On first
  connection of a new pubkey, get their username and make a new user.

### Challenges:

- Set limiting for users submitting answers in order to prevent
  brute-forcing
- What happens if someone overflows the attempts? -- Limit attempts to
  once per minute or an increasing amount of time

### Site Aesthetics:

- Make site look better: Organize CSS, apply css, etc.
- If possible, make the site as 'plain-text' as possible. Rely on
  mono-spaced font for challenge directions.

### Open-source-ize

- Simple configuration
- User plugin besides pulling yaml from http get
- SSH interface with list of "master users" who can use ssh to issue
  administrative commands like adding/removing users

### Misc:

- Make sure the database operations use minimal file permissions to
  open databases
- Put the databasename ("lab.db") in the configuration, and find a
  solution to host this longterm
- Have a generic "db" package that handles generic db operations like
  adding or returning all elements. There's way too much code
  duplication!
