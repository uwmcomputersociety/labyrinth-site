package routers

import (
	"github.com/astaxie/beego"
	"gitlab.com/uwmcomputersociety/labyrinth-site/controllers"
)

func init() {
	beego.Router("/", &controllers.MainController{})

	beego.Router("/about", &controllers.MainController{}, "get:About")

	beego.Router("/challenges", &controllers.MainController{}, "get:Challenges")
	beego.Router("/challenges/:challenge", &controllers.MainController{}, "get:Challenges")

	beego.Router("/leaderboard", &controllers.MainController{}, "get:Leaderboard")

}
