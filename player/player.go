package player

import (
	"errors"
	"github.com/astaxie/beego"
	"github.com/boltdb/bolt"
	"gitlab.com/uwmcomputersociety/labyrinth-site/challenge"
	"gopkg.in/yaml.v2"
	"time"
)

// How a player has challenges
type PlayerChallenge struct {
	Number      int
	Completed   bool
	Attempts    int
	LastAttempt int64 // unix timestamp
}

type LeaderboardEntry struct {
	Name  string
	Score int
	Place int
}

var ErrAlreadySolved = errors.New("the challenge has already been solved by this player")
var ErrNoChallenge = errors.New("the player has not attempted the challenge or it does not exist")
var ErrPlayerDoesNotExist = errors.New("The player accessed in the database doees not exist")

// Create PLAYERS bucket
func Init() error {

	PlayerDatabase, err := bolt.Open("lab.db", 0600, &bolt.Options{Timeout: 2 * time.Second})
	err = PlayerDatabase.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte("PLAYERS"))
		return err
	})
	defer PlayerDatabase.Close()

	return err
}

// Adds an attempt by user to challenge specified by number
// Increments the "Attempts," and marks as solved if it's solved.
// returns a non-nil error if there's any problems.
func AddAttempt(uid string, challenge int, solved bool) error {

	PlayerDatabase, err := bolt.Open("lab.db", 0600, &bolt.Options{Timeout: 2 * time.Second})
	if err != nil {
		beego.Error("error opening db", err)
		return err
	}
	defer PlayerDatabase.Close()

	err = PlayerDatabase.Update(func(tx *bolt.Tx) error {
		beego.Debug("Beginning viewing playerdb")
		// This bucket will be full of UIDs which are buckets of challenges.
		// These challenges are PlayerChallenge objects referenceed by challenge number
		b, err := tx.CreateBucketIfNotExists([]byte("PLAYERS"))
		if err != nil {
			beego.Warning(err)
			return err
		}
		p, err := b.CreateBucketIfNotExists([]byte(uid)) // Create/reference player bucket

		var storechallenge PlayerChallenge

		c := p.Get([]byte(string(challenge)))
		if c == nil { // no challenge created yet
			storechallenge = PlayerChallenge{
				Number:      challenge,
				Completed:   solved,
				Attempts:    1,
				LastAttempt: time.Now().UnixNano(),
			}
		} else { // Already attempted challenge
			err = yaml.Unmarshal(c, &storechallenge)
			if err != nil {
				beego.Error("error unmarshalling challenge:", err)
				return err
			}

			// If the player has already solved the challenge, return err
			if storechallenge.Completed == true {
				return ErrAlreadySolved
			} else {
				storechallenge.Completed = solved
				storechallenge.Attempts++
				storechallenge.LastAttempt = time.Now().UnixNano()
			}
		}

		// Remarshal and put yaml
		challengebytes, err := yaml.Marshal(storechallenge)
		if err != nil {
			beego.Error("error marshalling new challenge into db")
		}
		err = p.Put([]byte(string(challenge)), challengebytes)

		return err
	})

	if err != nil {
		beego.Warning(err)
	}

	return err
}

// Given a player, gets all their challenges they have attempted.
func GetPlayerChallenges(uid string) ([]PlayerChallenge, error) {

	var challenges []PlayerChallenge

	db, err := bolt.Open("lab.db", 0600, &bolt.Options{Timeout: 2 * time.Second})
	if err != nil {
		beego.Error("error opening db", err)
		return challenges, err
	}
	defer db.Close()

	err = db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("PLAYERS"))
		p := b.Bucket([]byte(uid))
		if err != nil {
			return err
		}

		var chal PlayerChallenge
		if p == nil { //No such player
			return ErrPlayerDoesNotExist
		}

		err = p.ForEach(func(k, v []byte) error {
			err = yaml.Unmarshal(v, &chal)
			if err != nil {
				beego.Error("error unmarshalling challenge:", err)
				return err
			}
			challenges = append(challenges, chal)
			return err
		})
		return err
	})

	return challenges, err

}

// Returns a PlayerChalenge struct by player uid and challengenum.
func GetPlayerChallenge(uid string, challengenum int) (PlayerChallenge, error) {
	var challenge PlayerChallenge
	db, err := bolt.Open("lab.db", 0600, &bolt.Options{Timeout: 2 * time.Second})
	if err != nil {
		beego.Error("Error opening db", err)
		return challenge, err
	}
	defer db.Close()

	err = db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("PLAYERS"))
		p := b.Bucket([]byte(uid))
		if err != nil {
			return err
		}

		c := p.Get([]byte(string(challengenum)))
		if c == nil {
			return ErrNoChallenge
		}

		err = yaml.Unmarshal(c, &challenge)
		if err != nil {
			beego.Error("error unmarshalling challenge:", err)
			return err
		}
		return nil

	})

	return challenge, err
}

// Go through all users and return a map of uid:PlayerChallenge
func GetChallenges(challengenum int) (map[string]PlayerChallenge, error) {

	uidmap := make(map[string]PlayerChallenge)
	db, err := bolt.Open("lab.db", 0600, &bolt.Options{Timeout: 2 * time.Second})
	if err != nil {
		beego.Error("Error opening db", err)
		return uidmap, err
	}
	defer db.Close()

	err = db.View(func(tx *bolt.Tx) error {

		var challenge PlayerChallenge
		b := tx.Bucket([]byte("PLAYERS"))
		c := b.Cursor()

		for k, _ := c.First(); k != nil; k, _ = c.Next() {
			u := b.Bucket([]byte(k))

			challengebytes := u.Get([]byte(string(challengenum)))
			if challengebytes == nil {
				beego.Debug("Challengenum", challengenum, "not found")
				continue
			}
			err = yaml.Unmarshal(challengebytes, &challenge)
			if err != nil {
				beego.Error("error unmarshalling challenge", err)
				return err
			}

			uidmap[string(k)] = challenge
		}

		return err
	})
	beego.Debug(uidmap)

	return uidmap, err

}

// Given a challenge number, returns all people that have completed the challenge.
func GetCompletedChallenges(challengenum int) (map[string]PlayerChallenge, error) {

	uidmap, err := GetChallenges(challengenum)
	if err != nil {
		return uidmap, err
	}

	retmap := make(map[string]PlayerChallenge)
	for k, v := range uidmap {
		if v.Completed == false {
			retmap[k] = v
		}
	}

	return retmap, err

}

// Given a player uid, returns the player's score
// A player's score is the sum of the difficulty of all completed challenges.
func GetPlayerScore(uid string) (int, error) {
	var pcs []PlayerChallenge
	pcs, err := GetPlayerChallenges(uid)
	if err == ErrPlayerDoesNotExist {
		// No problem
	} else if err != nil {
		beego.Error("Error in GetPlayerScore:", err)
		return -1, err
	}

	score := 0
	for _, pc := range pcs {
		c, err := challenge.GetChallenge(pc.Number)
		if err == challenge.ErrChallengeNotFound {
			beego.Warning("GetPlayerScore found a playerchallenge that wasn't a challenge??")
			continue
		} else if err != nil {
			beego.Error(err)
			return score, err
		}

		if pc.Completed {
			score += c.Difficulty
		}
	}

	return score, nil

}
