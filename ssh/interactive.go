package ssh

import (
	"io"
	ssht "golang.org/x/crypto/ssh/terminal"
	"github.com/astaxie/beego"
	"github.com/gliderlabs/ssh"
)

func interactiveprompt (s ssh.Session) {

	io.WriteString(s, `
 /\ /\ /\ /\ /\ /\
+--+--+--+--+--+--+
|  |     |  |     |
+-|THE LABYRINTH|-+
|     |  |     |  | 
+--+--+  +--+--+  +
|     |  |        |
+  +--+  +--+  +--+
|           |     |
+/\+  +--+--+--+  +
|  |     |        |
+  +--+--+--+--+--+

COMMANDS
 answer [QUESTION] [ANSWER]: Answers a challenge
 challenges: Shows all challenges
 challenge [ARG]: Shows challenge associated with number or name
 players: Shows all players with score
 leaderboard: Shows the full leaderboard
 help: Shows this

`)

	// This is here until I figure this shit out
	io.WriteString(s, "This functionality is currently disabled. Please give arguments to the ssh command to submit answers.\n")
	return

	// Interactive loop

	term := ssht.NewTerminal(s, "> ")

	for {

		in, err := term.ReadLine()

		if err != nil {
			io.WriteString(s, "Error parsing input")
			beego.Warning("Problem reading user input")
			continue
		}

		swrite(term, in)


	}
}

func swrite(t *ssht.Terminal, str string) (n int, err error) {

	n, err = t.Write([]byte(str + "\n"))
	return n, err

}
