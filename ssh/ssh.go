package ssh

import (
	"fmt"
	"github.com/astaxie/beego"
	"github.com/gliderlabs/ssh"
	"gitlab.com/uwmcomputersociety/labyrinth-site/users"
	gossh "golang.org/x/crypto/ssh"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"strconv"
	"time"
)

var sshserverport string = beego.AppConfig.String("sshserverport")
var hostkey string = beego.AppConfig.String("keypath")
var authorizedkeyspath = beego.AppConfig.String("authorizedkeyspath")

func ServerInit() {

	beego.Info("SSH server started, listening on", sshserverport)
	rand.Seed(time.Now().UnixNano())

	// User authentication
	pubhandle := func(ctx ssh.Context, key ssh.PublicKey) bool {

		u := users.GetUserByPubkey(key)
		if u.Uid == "" { // user isn't in db
			beego.Info(fmt.Sprintf("%s@%s denied connection to %s, not in db", ctx.User(), ctx.RemoteAddr(), ctx.LocalAddr()))
			return false
		}

		if u.State == "present" {
			ctx.SetValue("uid", u.Uid)
			beego.Info(fmt.Sprintf("%s@%s connected to %s as %s", ctx.User(), ctx.RemoteAddr(), ctx.LocalAddr(), u.Name))
			return true
		} else {
			beego.Info(fmt.Sprintf("%s@%s denied connection to %s, account disabled (%s)", ctx.User(), ctx.RemoteAddr(), ctx.LocalAddr(), u.Name))
			return false
		}

	}

	sshhandler := func(s ssh.Session) {
		if s.PublicKey() == nil {
			io.WriteString(s, "Please use a verified pubkey.\n")
			return
		}
		//authorizedKey := gossh.MarshalAuthorizedKey(s.PublicKey())

		in := s.Command() // Challenge will be in[0], answer will be in[1]
		if len(in) == 0 { // Interactive Prompt!!
			interactiveprompt(s)
			return
		} else if len(in) != 2 {
			io.WriteString(s, fmt.Sprintf("[ERR] Your submission is in an incorrect format. You need to enter 2 arguments instead of %d\n. Otherwise, use no arguments for an interactive prompt.", len(in)))
			io.WriteString(s, "Please enter the challenge number followed by your flag as an argument to the ssh call.\n")
			io.WriteString(s, "Example: 'ssh 1.2.3.4 -p 3997 4 thisisaflag'\n")
			return
		}
		chal, err := strconv.ParseInt(in[0], 10, 32)
		if err != nil {
			io.WriteString(s, fmt.Sprintf("[ERR] %s is not a number. Please refer to the challenges by their number when submitting answers.\n", in[0]))
			io.WriteString(s, "For example if the challenge is number 4 and you think the answer is 'thisisaflag', you would enter:\n")
			io.WriteString(s, "\tssh 1.2.3.4 -p 3997 4 thisisaflag\n")
			return
		}

		ans := in[1]
		io.WriteString(s, "Your submission has been sent to the grandwizard.\n")
		io.WriteString(s, fmt.Sprintf("Challenge:\t%d\n", chal))
		io.WriteString(s, fmt.Sprintf("Your Answer:\t%s\n", ans))
		io.WriteString(s, fmt.Sprintf("Evaluating"))

		randnum := rand.Intn(7) + 3
		for i := 1; i < randnum; i++ {
			time.Sleep(time.Millisecond * 500)
			io.WriteString(s, ".")
			time.Sleep(time.Millisecond * 300)
		}

		playeruid := s.Context().Value("uid").(string)
		response, err := HandleResponse(playeruid, int(chal), ans)
		if err != nil {
			beego.Error("Error handling labyrinth response:", err)
		}

		io.WriteString(s, fmt.Sprintf("\n%s", response))

	}

	s := &ssh.Server{
		Addr:             fmt.Sprintf(":%s", sshserverport),
		Handler:          sshhandler,
		PublicKeyHandler: pubhandle,
	}

	b, err := ioutil.ReadFile(hostkey)
	if err != nil {
		fmt.Printf("[ERR] reading or opening file: %v", err)
	}

	sng, err := gossh.ParsePrivateKey(b)
	if err != nil {
		fmt.Printf("We have a problem with signing: %v", err)
	}
	s.AddHostKey(sng)

	log.Fatal(s.ListenAndServe())

}
