package ssh

import (
	"fmt"
	"github.com/astaxie/beego"
	"github.com/dustin/go-humanize"
	"gitlab.com/uwmcomputersociety/labyrinth-site/challenge"
	"gitlab.com/uwmcomputersociety/labyrinth-site/player"
)

func HandleResponse(uid string, chal int, ans string) (string, error) {

	c, err := challenge.GetChallenge(chal)
	if err == challenge.ErrChallengeNotFound {
		return "Challenge not found.\n", nil
	} else if err != nil {
		return "PROBLEMS!", err
	}

	trueanswer, err := challenge.GetAnswer(chal)
	if err != nil {
		return "Error decrypting answer. Try again later!", err
	}

	solved := (trueanswer == ans)
	err = player.AddAttempt(uid, chal, solved)
	if err == player.ErrAlreadySolved {
		return fmt.Sprintf("You have already solved challenge %d.\n", chal), nil
	} else if err != nil {
		beego.Error("error handling response:", err)
		return fmt.Sprintf("Something went wrong: uid:%s chal:%s ans:%s\n", uid, chal, ans), err
	} else {
		att, err := player.GetPlayerChallenge(uid, chal)
		if err != nil {
			beego.Warning("Something wrong with getting challenge:", err)
			return fmt.Sprintf("Incorrect, try again."), nil
		} else if solved == false {
			return fmt.Sprintf("Incorrect, try again. This is your %s attempt.\n", humanize.Ordinal(att.Attempts)), nil
		} else {
			return fmt.Sprintf("Your correct answer has appeased the wizard! You have earned %d points.\n", c.Difficulty), nil
		}
	}

}
